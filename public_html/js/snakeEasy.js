/* -----------------------------------------------------------------------------
 * VARIABLES
 * -----------------------------------------------------------------------------
 */
var snek;
var snekLength;
var snekSize;
var snekDirection;

var fud;

var context;
var screenWidth;
var screenHeight;

var gameState;
var gameOverMenu;
var restartButton;
var playHUD;
var scoreboard;

var XboxSound;
var FailSound;

/*------------------------------------------------------------------------------
 * Executing Game Code
 * -----------------------------------------------------------------------------
 */

gameInitialize();
snekInitialize();
fudInitialize();
setInterval(gameLoop, 1000 / 50);

/*------------------------------------------------------------------------------
 * Game Functions
 * -----------------------------------------------------------------------------
 */

function  gameInitialize() {
    var canvas = document.getElementById("game-screen");
    context = canvas.getContext("2d");
    screenWidth = window.innerWidth;
    screenHeight = window.innerHeight;
    canvas.width = screenWidth;
    canvas.height = screenHeight;

    document.addEventListener("keydown", keyboardHandler);

    gameOverMenu = document.getElementById("GameOver");
    centerMenuPosition(gameOverMenu);

    restartButton = document.getElementById("restartButton");
    restartButton.addEventListener("click", gameRestart);

    playHUD = document.getElementById("playHUD");
    scoreboard = document.getElementById("scoreboard");
    
    XboxSound = new Audio("Sounds/Xbox");
    XboxSound.preload = "auto";
     
     FailSound = new Audio("Sounds/Fail");
     FailSound.preload = "auto";

    setState("PLAY");
}

function gameLoop() {
    gameDraw();
    drawScoreboard();
    if (gameState == "PLAY") {
        snekUpdate();
        snekDraw();
        fudDraw();
    }
}

function gameDraw() {
    context.fillStyle = "rgb(124,173,114)";
    context.fillRect(0, 0, screenWidth, screenHeight);
}
function gameRestart() {
    snekInitialize();
    fudInitialize();
    hideMenu(gameOverMenu);
    setState("PLAY");
}

/*------------------------------------------------------------------------------
 * Snek Functions
 * -----------------------------------------------------------------------------
 */

function snekInitialize() {
    snek = [];
    snekLength = 2;
    snekLength = 1;
    snekSize = 20;
    snekDirection = "down";

    for (var index = snekLength - 1; index >= 0; index--) {
        snek.push({
            x: index,
            y: 0
        });
    }
}

function snekDraw() {
    for (var index = 0; index < snek.length; index++) {
        context.fillStyle = "purple";
        context.fillRect(snek[index].x * snekSize, snek[index].y * snekSize, snekSize, snekSize);
    }
}

function snekUpdate() {
    var snekHeadX = snek[0].x;
    var snekHeadY = snek[0].y;


    if (snekDirection == "down") {
        snekHeadY++;
    }

    else if (snekDirection == "right") {
        snekHeadX++;
    }

    else if (snekDirection == "left") {
        snekHeadX--;
    }

    else if (snekDirection == "up") {
        snekHeadY--;
    }

    checkFudCollision(snekHeadX, snekHeadY);
    checkWallCollision(snekHeadX, snekHeadY);
    checkSnekCollision(snekHeadX, snekHeadY);


    var snekTail = snek.pop();
    snekTail.x = snekHeadX;
    snekTail.y = snekHeadY;
    snek.unshift(snekTail);
}

/*------------------------------------------------------------------------------
 * Fud Functions 
 * -----------------------------------------------------------------------------
 */

function fudInitialize() {
    fud = {
        x: 0,
        y: 0
    };
    setFudPosition();
}

function fudDraw() {
    context.fillStyle = "white";
    context.fillRect(fud.x * snekSize, fud.y * snekSize, snekSize, snekSize);
}

function setFudPosition() {
    var randomX = Math.floor(Math.random() * screenWidth);
    var randomY = Math.floor(Math.random() * screenHeight);

    fud.x = Math.floor(randomX / snekSize);
    fud.y = Math.floor(randomY / snekSize);
}

/*------------------------------------------------------------------------------
 * KeyBoardStuff
 * -----------------------------------------------------------------------------
 */
function keyboardHandler(event) {
    console.log(event);

    if (event.keyCode == "39" && snekDirection != "left") {
        snekDirection = "right";
    }
    else if (event.keyCode == "40" && snekDirection != "up") {
        snekDirection = "down";

    }
    else if (event.keyCode == "37" && snekDirection != "right") {
        snekDirection = "left";

    }
    else if (event.keyCode == "38" && snekDirection != "down") {
        snekDirection = "up";

    }
}

/*------------------------------------------------------------------------------
 * Collision Handling
 * -----------------------------------------------------------------------------
 */

function checkFudCollision(snekHeadX, snekHeadY) {
    if (snekHeadX == fud.x && snekHeadY == fud.y) {
        snek.push({
            x: 0,
            y: 0
        });
        snekLength++;
        setFudPosition();
        XboxSound.play();
    }
}

function checkWallCollision(snekHeadX, snekHeadY) {
    if (snekHeadX * snekSize >= screenWidth || snekHeadX * snekSize < 0) {
        setState("GAME OVER");
         FailSound.play();
    }
    else if (snekHeadY * snekSize >= screenHeight || snekHeadY * snekSize < 0) {
        setState("GAME OVER");
         FailSound.play();
    }
}

function checkSnekCollision(snekHeadX, snekHeadY) {
    for (var index = 1; index < snek.length; index++) {
        if (snekHeadX == snek[index].x && snekHeadY == snek[index].y) {
            setState("GAME OVER");
            return;
        }
 
    }
}

/*----------------------------------------------------------------------------
 * Game State
 *----------------------------------------------------------------------------
 */

function setState(state) {
    gameState = state;
    showMenu(state);
}

/*------------------------------------------------------------------------------
 * Menu Functions
 *------------------------------------------------------------------------------
 */

function displayMenu(menu) {
    menu.style.visibility = "visible";
}


function hideMenu(menu) {
    menu.style.visibility = "hidden";
}
function showMenu(state) {
    if (state == "GAME OVER") {
        displayMenu(gameOverMenu);
    }
    else if (state == "PLAY") {
        displayMenu(playHUD);
    }
}

function centerMenuPosition(menu) {
        menu.style.top = (screenHeight / 2) - (menu.offsetHeight / 2) + "px";
        menu.style.left = (screenWidth / 2) - (menu.offsetWidth / 2) + "px";

    }

function drawScoreboard() {
    scoreboard.innerHTML = "Length: " + snekLength;
    }
    

